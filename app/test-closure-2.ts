(function(){
    let add = function (counter:number) {
        let c = counter; // la variabile 'c' insieme a 'counter' fa parte della closure della funzione restituita

        return function ():number {c += 1; return c}
    };
    
    let f1:()=>number = add(5);
    f1();
    let f2 = add(10);
    f2();
    let n = f1();
    let m = f2();

    console.log('m='+m+", n="+n)
})();

