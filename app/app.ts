
// Express
import express = require('express');

// CORS for Express
import cors = require('cors');

// MongoDB
import { Db, MongoClient } from 'mongodb';

// Assert 
const assert = require('assert');

// MongoDB connection URL, database and variables
const url: string = 'mongodb://localhost:27017';
const dbName:string = 'iot-db';
var mongoDBClient:MongoClient;
var db:Db;

// Create and initialise a new express application instance
const app: express.Application = express();
app.use(cors()) // CORS app web -> server web su dominio diverso rispetto a quello di download della pagina
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

// REST end-points
// ===============
app.get('/', function (req, res) {
  res.send('Hello Node World!');
});

app.post('/data/', function (req, res) {
  let body = req.body;

  if(mongoDBClient.isConnected()) {
    const collection = db.collection('data');
    collection.insertOne(body);
    res.send('OK!!!');
  }
  else {
    res.send('KO!');
  }

});

app.get('/test/', function (req:express.Request, res:express.Response) {
  res.send('Hello Test World!');
});


// Launch server on port 3000
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

function openDBConnection() {
  MongoClient.connect(url, { useUnifiedTopology: true }, function(err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to MongoDB server");
  
    mongoDBClient = client;
    db = client.db(dbName);
  });
}

// open connection
openDBConnection();

function closeDBConnection() {
  mongoDBClient.close();
  console.log("Closed MongoDB client");
}

setInterval(()=>{
  if(mongoDBClient!=undefined)
    console.log('MongoDB '+(mongoDBClient.isConnected()?'connected':'not connected'));
  else
    console.log('Waiting for connection of MongoDB');
}, 5000);


